import dotenv from 'dotenv';

const envFound = dotenv.config();
if (envFound.error) {
  throw new Error("Couldn't find .env file");
}

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

export default {
  NODE_ENV: process.env.NODE_ENV,
  PORT: Number(process.env.PORT || 8080),
  DOMAIN: process.env.DOMAIN,
  DIR_SWAGGER: 'src/config/docs/swagger.yml',
  DIR_ERRORS: 'src/config/errors/error.yml',
};
