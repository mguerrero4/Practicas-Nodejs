// Use this singleton to create connections to the database in use
class DatabaseConnection {
  private static instance: any;

  private constructor() {}

  static getInstance() {
    if (!DatabaseConnection.instance) {
      DatabaseConnection.instance = true;
    }
    return DatabaseConnection.instance;
  }
}

export default DatabaseConnection.getInstance();
