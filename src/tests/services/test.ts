import { expect, assert } from "chai";
import testService from "../../services/test.service";

describe("Test example", () => {
    it('Checking test', async () => {
        const response = await testService.testEndpoint();

        expect(response).to.be.an("Object");
        expect(response.status).to.be.equal(200);
        expect(response.message).to.be.a("String");
    });

    it("Checking test Object properties", async () => {
        const response = await testService.testEndpoint();

        expect(response).to.be.an("Object").to.have.property("message");
        assert(response);
        assert.deepEqual({status: 200}, {status: 200});
        assert.notDeepEqual({status: 200}, {status: 400});
        assert.isNotObject(response.message, "the message response is not an object");
    });
});