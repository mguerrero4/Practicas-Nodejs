import { Router } from 'express';
import testRoute from './test.route';
import userRoute from './user.route';
import exerciseRoute from './exercise.route';

const router = Router();

router.use(testRoute);
router.use(userRoute);
router.use(exerciseRoute)

export default router;
