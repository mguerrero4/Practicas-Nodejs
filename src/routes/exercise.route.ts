import { NextFunction, Request, Response, Router } from 'express';
import exerciseService from '../services/exercise.service';

class ExerciseRoute {
  public router = Router();

  constructor() {
    this.createRoutes();
  }

  createRoutes(): void {
    this.router.get('/calculate-total', this.getTotalReadings.bind(this));
    this.router.get('/read-readings', this.getReadReadings.bind(this));
  }

  getTotalReadings(req: Request, res: Response, next: NextFunction) {
    exerciseService.calculateTotal()
      .then((response) => res.json(response))
      .catch((err) => next(err));
  }

  getReadReadings(req: Request, res: Response, next: NextFunction) {
    exerciseService.getReadReadings()
      .then((response) => res.json(response))
      .catch((err) => next(err));
  }
}

export default new ExerciseRoute().router;