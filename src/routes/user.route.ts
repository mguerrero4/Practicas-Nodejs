import { NextFunction, Request, Response, Router } from 'express';
import UserService from '../services/user.service';

class UserRoute {
  public router = Router();

  constructor() {
    this.createRoutes();
  }

  createRoutes(): void {
    this.router.post('/user', this.addUser.bind(this));
  }

  addUser(req: Request, res: Response, next: NextFunction) {
    UserService.addUser(req.body)
      .then((response) => res.json(response))
      .catch((err) => next(err));
  }
}

export default new UserRoute().router;