class TestService {
  async testEndpoint() {
    return { status: 200, message: 'Successful operation' };
  }
}

export default new TestService();
