import getAge from '../utils/getAge';
import User from '../data/models/User';
import passEncripted from '../utils/encrypted';

class UserService {
  // agregar Usuario
  async addUser(user: User) {
    const usuario = new User(
      user.name,
      user.userName,
      user.password,
      user.age,
      user.createdAt,
      user.birthday,
      user.active
    );
    
    usuario.createdAt = new Date();
    usuario.password = await passEncripted(user.password);
    usuario.age = getAge(1)

    return usuario;
  }
}

export default new UserService();
