import reading_units from '../data/ru.json'
import readings from '../data/readings.json'

class ExerciseService {
  // El archivo que se importa es un arreglo de unidades de lecturas. El ejercicio consiste en SUMAR los TOTALES de lecturas que tiene cada unidad de lectura.
  // REGLAS:
  // No puede iterar mas de una vez
  // No puede usar if-else, for y foreach
  async calculateTotal(){
      return reading_units
  }
  // El archivo que se importa es un arreglo de lecturas. El ejercicio consiste en devolver como respuesta las lecutras LEIDAS.
  // Las lecturas se consideran leidas cuando la lectura contiene una nota de lectura (note) y/o contador (counter).
  // REGLAS:
  // No puede iterar mas de una vez
  // No se puede usar foreach ni else

  async getReadReadings(){
    return readings;
  }
}

export default new ExerciseService();
