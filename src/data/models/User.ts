export default class User {
  name: string;  
  
  userName: string;

  password: string;

  age: number;

  createdAt: Date;
  
  birthday: Date;

  active: boolean;

  constructor(
    name: string,
    userName: string,
    password: string,
    age: number,
    createdAt: Date,
    birthday: Date,
    active: boolean
  ) {
    this.name = name;
    this.userName = userName;
    this.password = password;
    this.age = age;
    this.createdAt = createdAt;
    this.birthday = birthday;
    this.active = active;
  }
}
