import bcryptjs from 'bcryptjs';

export default function passEncripted(pass: string) {
  return bcryptjs.hash(pass, 8);
}
