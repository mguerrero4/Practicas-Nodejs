FROM node:lts-alpine3.13 as builder
ENV NODE_ENV=prod

# Select workdir
WORKDIR  /app

# Copy code
COPY . /app

# Build application 
RUN npm install
RUN npm run build

# Image
FROM node:lts-alpine3.13

# Select workdir
WORKDIR /usr/src/app

# Install tools and change permissions
RUN apk add --no-cache tini \
    && chown node:node /usr/src/app 

# Add artifacts from builder image
COPY  --chown=node:node --from=builder /app/.env /usr/src/app/
COPY  --chown=node:node --from=builder /app/dist /usr/src/app/src
COPY  --chown=node:node --from=builder /app/node_modules /usr/src/app/node_modules

# Change to no-root user
USER node 
RUN    mkdir -p /usr/src/app/logs \
#       && ln -sf /usr/src/app/logs/server.log /dev/stdout
       && ln -sf /dev/stdout /usr/src/app/logs/server.log

# Init Manager for node process
ENTRYPOINT ["/sbin/tini", "--"]

# Run application
CMD [ "node", "src/index.js" ]

# Application port
EXPOSE 8080
